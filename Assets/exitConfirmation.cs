﻿using UnityEngine;
using System.Collections;

public class exitConfirmation : MonoBehaviour {

#if UNITY_ANDROID

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            MobileNativeDialog dialog = new MobileNativeDialog("CONFIRM EXIT", "Do you want to quit the game?");
            //   Application.Quit(); 
            Time.timeScale = 0;

            dialog.OnComplete += OnDialogClose;
        }
    }
#endif
    private void OnDialogClose(MNDialogResult result)
    {
        switch (result)
        {
            case MNDialogResult.YES:
                //Debug.Log("Yes button pressed"); 
                Application.Quit();
                break;
            case MNDialogResult.NO:
                Debug.Log("No button pressed");
                Time.timeScale = 1;
                break;
        }
    }

}
